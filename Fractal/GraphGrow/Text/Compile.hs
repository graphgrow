module Fractal.GraphGrow.Text.Compile (compileText) where

import Control.Monad (forM_)
import Control.Monad.Identity (Identity, runIdentity)
import Control.Monad.Supply (SupplyT, evalSupplyT, supply)
import Control.Monad.State (StateT, execStateT, get, put)
import Control.Monad.Writer (WriterT, runWriterT, tell)
import Control.Monad.Trans (lift)
import Data.Map (Map)
import qualified Data.Map as M

import Fractal.GraphGrow.Engine.Geometry (Transform)
import Fractal.GraphGrow.Engine.Graph (MGraph(..), Node(..))
import Fractal.GraphGrow.Text.Glyphs (string, char)

-- the input
type Specification = [(String, [String])]

-- the output
type Environment = Map String Node

-- intermediate data
type PreGraph = [(Node, [(Node, Transform)])]

-- the main compiler monad stack
type Compile = StateT Environment (WriterT PreGraph (SupplyT Node Identity))

runCompile :: Compile () -> (Environment, MGraph)
runCompile = runIdentity . supplyingNodes . writingGraph . trackingEnvironment
  where
    supplyingNodes m = evalSupplyT m $ map Node [0..]
    writingGraph m = fmap (MGraph . M.fromListWith (++)) `fmap` runWriterT m
    trackingEnvironment m = execStateT m M.empty

-- allocate stable nodes for strings
namedNode :: String -> Compile Node
namedNode s = do
  m <- get
  case M.lookup s m of
    Nothing -> do
      n <- supply
      put $ M.insert s n m
      return n
    Just n -> do
      return n

-- allocate unique nodes for characters
node :: Compile Node
node = supply

-- add an edge to the directed graph
edge :: Node -> Node -> Transform -> Compile ()
edge from to transform = tell [(from, [(to, transform)])]

-- the main compiler program
-- maps each stroke of each character in each "source" string..
-- .. to one of that source's "target" strings
compileText :: Specification -> (Environment, MGraph)
compileText graph = runCompile $ do
  forM_ graph $ \(source, targets) -> do
    sourceNode <- namedNode source
    flip evalSupplyT (cycle targets) $ do
      -- re-using supply makes sense, but either
      --   a) there are a lot of 'lift's (as in this code)
      --   b) needs orphan instances to automatigally lift
      forM_ (source `zip` string (length source)) $ \(sourceChar, charTransform) -> do
        charNode <- lift node
        lift $ edge sourceNode charNode charTransform
        forM_ (char sourceChar) $ \strokeTransform -> do
          target <- supply
          targetNode <- lift $ namedNode target
          lift $ edge charNode targetNode strokeTransform
