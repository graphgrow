module Fractal.GraphGrow.Text.Parse (parseText) where

parseText :: String -> Maybe [(String, [String])]
parseText = mapM (uncons . words) . lines
  where
    uncons (w:_:ws) = Just (w, ws)
    uncons _ = Nothing
