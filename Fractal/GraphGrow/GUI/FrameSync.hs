module Fractal.GraphGrow.GUI.FrameSync
  ( FrameSyncOptions(..)
  , defaultFrameSyncOptions
  , newFrameSync
  , FrameSync
  , Deadline(..)
  , Statistics(..)
  ) where

import Prelude hiding ((++))

import Control.Monad (replicateM_)
import Data.IORef (newIORef, readIORef, writeIORef)
import Data.List (foldl')
import Data.Monoid (Monoid(..))

import Fractal.GraphGrow.Analysis.Statistics hiding (s0)

data Statistics = Statistics
  { statFrameTime :: !(StatOrd Double)
  , statSpareTime :: !(StatOrd Double)
  , statOk        :: !(StatOrd Double)
  , statMissed    :: !Int
  , statCount     :: !Int
  }

instance Monoid Statistics where
  mempty = let e = mempty in Statistics e e e 0 0
  Statistics a b c d e `mappend` Statistics s t u v w =
    let (++) = mappend in Statistics (a++s)(b++t)(c++u)(d+v)(e+w)
  mconcat = foldl' mappend mempty

data Deadline = Missed | Ok !Double
  deriving (Read, Show, Eq, Ord)

type FrameSync = IO (Deadline, Statistics)

data FrameSyncOptions = FrameSyncOptions
  { frameSyncMeasure  :: Int
  , frameSyncMultiple :: Int
  , frameSyncMissed   :: Double
  , frameSyncTarget   :: Double
  }

defaultFrameSyncOptions :: FrameSyncOptions
defaultFrameSyncOptions = FrameSyncOptions
  { frameSyncMeasure  = 60
  , frameSyncMultiple = 1
  , frameSyncMissed   = 1.5
  , frameSyncTarget   = 0.75
  }

newFrameSync :: IO Double -> IO () -> FrameSyncOptions -> IO (Double, FrameSync)
newFrameSync getTime swapBuffers opts = do
  frameInterval <- measureFrameInterval getTime swapBuffers (frameSyncMeasure opts)
  statRef <- newIORef mempty
  frameTimeRef <- newIORef =<< getTime
  let interval = fromIntegral (frameSyncMultiple opts) * frameInterval
      frameSync = do
        lastTime <- readIORef frameTimeRef
        preTime <- getTime
        swapBuffers
        frameTime <- getTime
        writeIORef frameTimeRef frameTime
        s0 <- readIORef statRef
        let s = s0{ statFrameTime = statFrameTime s0 `mappend` statOrd (frameTime - lastTime)
                  , statSpareTime = statSpareTime s0 `mappend` statOrd (frameTime - preTime )
                  , statCount     = statCount s0 + 1
                  }
            missed = frameTime - lastTime > interval * frameSyncMissed opts
            delta = (frameTime - preTime) / (interval * (1 - frameSyncTarget opts))
            result@(_, s')
              | missed    = (Missed,   s{ statMissed = statMissed s + 1 })
              | otherwise = (Ok delta, s{ statOk = statOk s `mappend` statOrd delta })
        writeIORef statRef s'
        return result
  return (recip interval, frameSync)

measureFrameInterval :: IO Double -> IO () -> Int -> IO Double
measureFrameInterval getTime swapBuffers count = do
  swapBuffers
  startTime <- getTime
  replicateM_ count swapBuffers
  endTime <- getTime
  return $ (endTime - startTime) / fromIntegral count
