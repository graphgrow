#version 130
uniform float er;
uniform float rho;

void main() {
  vec2 p = er * (gl_TexCoord[0].xy * 2.0 - vec2(1.0));
  float l = length(p);
  float n;
  if (l >= er) {
    n = 0.0;
  } else if (er > l && l >= rho * er) {
    n = (log(er) - log(l)) / -log(rho);
  } else {
    n = -1.0;
  }
  gl_FragData[0] = vec4(n, 1.0, 0.0, 0.0);
}
