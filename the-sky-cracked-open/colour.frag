#version 130
uniform sampler2DArray src;
uniform float er;
uniform float rho;
uniform float pixelSize;
uniform float speed;

void main() {
  vec2 p = gl_TexCoord[0].xy;
  vec2 z = texture(src, vec3(p, 0.0)).xy;
/*
  float zk = er * pow(1.0/rho, fract(z.x));
  float d = zk * log(zk) / z.y;
  float g = tanh(clamp(d / length(dFdx(p)), 0.0, 4.0));
  gl_FragData[0] = vec4(vec3(g), 1.0);
*/
  float n = z.x * speed;
  vec3 yuv;
  if (n < 0.0) {
    yuv = vec3(0.0);
  } else {
    yuv = vec3(clamp(log(1.0 + n / 4.0), 0.0, 1.0), 0.125 * sin(n), -0.125 * cos(n));
  }
  vec3 rgb = yuv * mat3(1.0, 0.0, 1.4, 1.0, -0.395, -0.581, 1.0, 2.03, 0.0);
  gl_FragData[0] = vec4(rgb, 1.0);
}
