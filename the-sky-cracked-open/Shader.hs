module Shader (shader) where

import Graphics.Rendering.OpenGL

shader :: Maybe FilePath -> Maybe FilePath -> IO Program
shader mV mF = do
  p <- createProgram
  vs <- case mV of
    Nothing -> return []
    Just v -> do
      vert <- createShader VertexShader
      source <- readFile v
      shaderSource vert $= [ source ]
      compileShader vert
      print =<< get (shaderInfoLog vert)
      return [vert]
  fs <- case mF of
    Nothing -> return []
    Just f -> do
      frag <- createShader FragmentShader
      source <- readFile f
      shaderSource frag $= [ source ]
      compileShader frag
      print =<< get (shaderInfoLog frag)
      return [frag]
  attachedShaders p $= (vs ++ fs)
  linkProgram p
  print =<< get (programInfoLog p)
  return p
