{-# LANGUAGE PatternSynonyms #-}
import Control.Monad (when, replicateM_, forM_)
import System.Exit (exitSuccess)
import Graphics.UI.GLUT hiding (FramebufferObject)
import Graphics.GL
  ( glTexImage3D, pattern GL_TEXTURE_2D_ARRAY, pattern GL_RG32F, pattern GL_RG
  , pattern GL_UNSIGNED_BYTE, pattern GL_FALSE, glClampColor
  , pattern GL_CLAMP_VERTEX_COLOR, pattern GL_CLAMP_READ_COLOR, pattern GL_CLAMP_FRAGMENT_COLOR
  , glGenFramebuffers, glBindFramebuffer, glFramebufferTextureLayer, glTexParameteri
  , pattern GL_FRAMEBUFFER, pattern GL_COLOR_ATTACHMENT0, glUniformMatrix3fv
  , GLuint, GLint, GLfloat, GLdouble, glUniform1fv, glBindTexture
  , pattern GL_TEXTURE_MIN_FILTER, pattern GL_TEXTURE_MAG_FILTER, pattern GL_LINEAR
  )
import Foreign (alloca, peek, nullPtr, withArray)
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import Numeric.GSL.Root
import qualified Numeric.LinearAlgebra as L
import Numeric.LinearAlgebra hiding (R, dim, size)
import Unsafe.Coerce (unsafeCoerce)

import Shader (shader)
import Snapshot (writeSnapshot)

mXm :: L.Matrix R -> L.Matrix R -> L.Matrix R
mXm = (<>)

size :: Int
size = 2048

er :: R
er = 4

data St = St
  { pInitial :: Program, pInitial'er, pInitial'rho :: UniformLocation
  , pStep :: Program, pStep'er, pStep'count, pStep'ts, pStep'ws, pStep'ls, pStep'layer, pStep'src :: UniformLocation
  , pColour :: Program, pColour'src, pColour'er, pColour'rho, pColour'pixelSize, pColour'speed :: UniformLocation
  , tPing, tPong :: TextureObject
  , fBuffer :: FramebufferObject
  , vTrs :: [[L.Matrix R]], vLs :: [[R]], vT :: Int, vPasses :: Int, vRho :: (R, R)
  }

main :: IO ()
main = do
  _ <- getArgsAndInitialize
  initialWindowSize $= Size 1280 720
  _ <- createWindow "spiral!"
  pInitial'     <- shader Nothing (Just "initial.frag")
  pInitial'er'  <- get $ uniformLocation pInitial' "er"
  pInitial'rho' <- get $ uniformLocation pInitial' "rho"
  pStep'        <- shader Nothing (Just "step.frag")
  pStep'er'     <- get $ uniformLocation pStep' "er"
  pStep'count'  <- get $ uniformLocation pStep' "count"
  pStep'ts'     <- get $ uniformLocation pStep' "transform"
  pStep'ws'     <- get $ uniformLocation pStep' "source"
  pStep'ls'     <- get $ uniformLocation pStep' "scaleFactor"
  pStep'layer'  <- get $ uniformLocation pStep' "layer"
  pStep'src'    <- get $ uniformLocation pStep' "src"
  pColour'      <- shader Nothing (Just "colour.frag")
  pColour'src'  <- get $ uniformLocation pColour' "src"
  pColour'er'   <- get $ uniformLocation pColour' "er"
  pColour'rho'  <- get $ uniformLocation pColour' "rho"
  pColour'pixelSize' <- get $ uniformLocation pColour' "pixelSize"
  pColour'speed'<- get $ uniformLocation pColour' "speed"
  tPing'        <- newTex size
  tPong'        <- newTex size
  fBuffer'      <- newFBO
  glClampColor GL_CLAMP_VERTEX_COLOR   (fromIntegral GL_FALSE)
  glClampColor GL_CLAMP_READ_COLOR     (fromIntegral GL_FALSE)
  glClampColor GL_CLAMP_FRAGMENT_COLOR (fromIntegral GL_FALSE)
  sR <- newIORef St
    { pInitial = pInitial', pInitial'er = pInitial'er', pInitial'rho = pInitial'rho'
    , pStep = pStep', pStep'er = pStep'er', pStep'count = pStep'count', pStep'ts = pStep'ts', pStep'ws = pStep'ws', pStep'ls = pStep'ls', pStep'layer = pStep'layer', pStep'src = pStep'src'
    , pColour = pColour', pColour'src = pColour'src', pColour'er = pColour'er', pColour'rho = pColour'rho', pColour'pixelSize = pColour'pixelSize', pColour'speed = pColour'speed'
    , tPing = tPing', tPong = tPong', fBuffer = fBuffer'
    , vTrs = [[], []], vLs = [[], []], vT = 0, vPasses = 16, vRho = (0, 0)
    }
  reset sR
  displayCallback $= display sR
  addTimerCallback 100 (timer sR)
  mainLoop

timer :: IORef St -> IO ()
timer sR = do
  addTimerCallback 100 (timer sR)
  reset sR
  postRedisplay Nothing

reset :: IORef St -> IO ()
reset sR = do
  s <- readIORef sR
--  when (vT s > 0) $ writeSnapshot (show (vT s) ++ ".ppm") (Position 0 0) (Size 1280 720)
--  when (vT s >= 375) $ exitSuccess
  let dim1 = 1.001 + 0.75 * abs (sin(pi/2 * fromIntegral (vT s) / 30.0))
      dim2 = 1.001 + 0.75 * abs (sin(pi/2 * fromIntegral (vT s) / 37.5))
      spiral1 = spiral dim1
      spiral2 = spiral dim2
      ls = [map fst spiral1, map fst spiral2]
      rhos = concat ls
      mrhos = map maximum ls
      rho = (minimum rhos, maximum rhos)
      trs1 = map snd spiral1
      trs2 = map snd spiral2
      passes = clamp 16 4096 . round . logBase (snd rho) $ 0.001
  writeIORef sR s{ vTrs = [trs1, trs2], vLs = ls, vT = vT s + 1, vPasses = passes, vRho = rho }
  viewport $= (Position 0 0, Size (fromIntegral size) (fromIntegral size))
  loadIdentity
  ortho2D 0 1 0 1
  forM_ [0,1] $ \layer -> do
    bindFBO (fBuffer s) (tPing s) (fromIntegral layer)
    currentProgram $= Just (pInitial s)
    uniform (pInitial'er s) $= TexCoord1 (realToFrac er :: GLfloat)
    uniform (pInitial'rho s) $= TexCoord1 (realToFrac (mrhos !! layer) :: GLfloat)
    unitQuad
    currentProgram $= Nothing
    unbindFBO

clamp :: Ord a => a -> a -> a -> a
clamp mi ma x = mi `max` x `min` ma

display :: IORef St -> IO ()
display sR = do
  s0 <- readIORef sR
  replicateM_ (vPasses s0) $ do
    s <- readIORef sR
    viewport $= (Position 0 0, Size (fromIntegral size) (fromIntegral size))
    glBindTexture GL_TEXTURE_2D_ARRAY (case tPing s of TextureObject t -> t)
    currentProgram $= Just (pStep s)
    uniform (pStep'er s) $= TexCoord1 (realToFrac er :: GLfloat)
    uniform (pStep'src s) $= TexCoord1 (0 :: GLint)
    forM_ [0,1] $ \layer -> do
      bindFBO (fBuffer s) (tPong s) (fromIntegral layer)
      uniform (pStep'count s) $= TexCoord1 (3 :: GLint)
      withArray (map (realToFrac :: R -> GLfloat) . concat . concatMap L.toLists . (!! layer) . vTrs $ s) $ glUniformMatrix3fv (unsafeCoerce $ pStep'ts s) 3 1
      withArray (map (realToFrac :: R -> GLfloat) . (!! layer) . vLs $ s) $ glUniform1fv (unsafeCoerce $ pStep'ls s) 3
      withArray ([[0, 1, 0], [1, 0, 1 :: GLfloat]] !! layer) $ glUniform1fv (unsafeCoerce $ pStep'ws s) 3
      uniform (pStep'layer s) $= TexCoord1 (realToFrac layer :: GLfloat)
      unitQuad
      unbindFBO
    currentProgram $= Nothing
    glBindTexture GL_TEXTURE_2D_ARRAY 0
    writeIORef sR s{ tPing = tPong s, tPong = tPing s }

  s <- readIORef sR
  viewport $= (Position 0 0, Size 1280 720)
  glBindTexture GL_TEXTURE_2D_ARRAY (case tPong s of TextureObject t -> t)
  currentProgram $= Just (pColour s)
  uniform (pColour'src s) $= TexCoord1 (0 :: GLint)
  uniform (pColour'er s) $= TexCoord1 (realToFrac er :: GLfloat)
  uniform (pColour'rho s) $= TexCoord1 (realToFrac (fst $ vRho s) :: GLfloat)
  uniform (pColour'pixelSize s) $= TexCoord1 (0.1 :: GLfloat)
  uniform (pColour'speed s) $= TexCoord1 (2 * pi / fromIntegral (vPasses s) :: GLfloat)
  fullQuad
  currentProgram $= Nothing
  glBindTexture GL_TEXTURE_2D_ARRAY 0
  swapBuffers
  reportErrors

newTex :: Int -> IO TextureObject
newTex s = do
  [TextureObject t] <- genObjectNames 1
  glBindTexture GL_TEXTURE_2D_ARRAY t
  glTexImage3D GL_TEXTURE_2D_ARRAY 0 (fromIntegral GL_RG32F) (fromIntegral s) (fromIntegral s) 2 0 GL_RG GL_UNSIGNED_BYTE nullPtr
  glTexParameteri GL_TEXTURE_2D_ARRAY GL_TEXTURE_MIN_FILTER (fromIntegral GL_LINEAR)
  glTexParameteri GL_TEXTURE_2D_ARRAY GL_TEXTURE_MAG_FILTER (fromIntegral GL_LINEAR)
  glBindTexture GL_TEXTURE_2D_ARRAY 0
  return (TextureObject t)

newtype FramebufferObject = FramebufferObject GLuint

newFBO :: IO FramebufferObject
newFBO = fmap FramebufferObject (alloca $ \p -> glGenFramebuffers 1 p >> peek p)

bindFBO :: FramebufferObject -> TextureObject -> GLint -> IO ()
bindFBO (FramebufferObject f) (TextureObject t) layer = do
  glBindFramebuffer GL_FRAMEBUFFER f
  glFramebufferTextureLayer GL_FRAMEBUFFER GL_COLOR_ATTACHMENT0 t 0 layer

unbindFBO :: IO ()
unbindFBO = do
  glFramebufferTextureLayer GL_FRAMEBUFFER GL_COLOR_ATTACHMENT0 0 0 0
  glBindFramebuffer GL_FRAMEBUFFER 0

unitQuad :: IO ()
unitQuad = renderPrimitive Quads $ do
    t 0 1 >> v 0 1
    t 0 0 >> v 0 0
    t 1 0 >> v 1 0
    t 1 1 >> v 1 1
  where
    t, v :: GLdouble -> GLdouble -> IO ()
    t x y = texCoord (TexCoord2 x y)
    v x y = vertex (Vertex2 x y)

fullQuad :: IO ()
fullQuad = renderPrimitive Quads $ do
    t (0.5-tx) (0.5+ty) >> v 0 1
    t (0.5-tx) (0.5-ty) >> v 0 0
    t (0.5+tx) (0.5-ty) >> v 1 0
    t (0.5+tx) (0.5+ty) >> v 1 1
  where
    tx = 0.5 / er
    ty = 0.5 / er * 9 / 16
    t, v :: GLdouble -> GLdouble -> IO ()
    t x y = texCoord (TexCoord2 x y)
    v x y = vertex (Vertex2 x y)

type R = Double

transformRST :: R -> R -> L.Vector R -> L.Matrix R
transformRST a l p = (3><3)[ c, s, x, -s, c, y, 0, 0, 1 ]
  where x:y:_ = toList p
        c = l * cos a
        s = l * sin a

findZero :: (R -> R) -> R -> R
findZero f = head . fst . root Hybrids 1e-12 1000 ((:[]) . f . head) . (:[])

spiral :: R -> [(R, L.Matrix R)]
spiral d = [(l1, inv t1), (l2, inv t2), (l3, inv t3)]
  where
    f l = 2 * sqrt ((1 - l * l) / 4) ** d + l ** d - 1
    l1 = sin (acos l2) / 2
    l2 = findZero f (d - 1)
    l3 = l1
    a1 = pi / 2 - acos l2
    a2 = -acos l2
    a3 = a1
    x1 = 0
    y1 = 0
    x2 =  l1 * cos a1
    y2 = -l1 * sin a1
    x3 = 1 - x2
    y3 = 0 - y2
    v1 = 2 |> [ x1, y1 ]
    v2 = 2 |> [ x2, y2 ]
    v3 = 2 |> [ x3, y3 ]
    t1 = post `mXm` transformRST a1 l1 v1 `mXm` pre
    t2 = post `mXm` transformRST a2 l2 v2 `mXm` pre
    t3 = post `mXm` transformRST a3 l3 v3 `mXm` pre
    pre  = transformRST 0 1 (2|>[ 0.5,0])
    post = transformRST 0 1 (2|>[-0.5,0])
